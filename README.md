# Keyless TLS Terminator - NoKeyServer Deployment

This is a deployment guide for the KTT NoKeyServer

## Prerequesites

### Hardware/OS

KTT is tested against common Linux distributions. It requires a modest amount of CPU and RAM, but should not be starved. Of much more importance is network latency and cryptography performance.

### Network

KTT, while fast, is _incredibly_ dependent on the latency between the proxy server and the NoKeyServer deployment. It is recommended to separate the KTT NKS and KTT proxy by _at minimum_, user groups on Linux, however, it is completely possible to have the two services operating on opposite sides of the planet. However, TLS handshake times will be very sensitive to the network latency.

#### Ports

- Inbound TCP 7753 - Proxy to KTT NKS communication. This can be changed in the NKS configs.

### Software

#### systemd

There is a sample systemd service made available in this repository. It is available [here](https://gitlab.com/space55/keyless-tls-server-deployment/-/blob/main/ktt-nks.service)

## Deploying

1. Install KTT to your system, in `/usr/local/bin`.
1. Create a user `ktt-nks`.
1. Create a directory at `/etc/ktt`, and make it readable to the user `ktt-nks`.
1. Copy the `nks.toml.example` file to `/etc/ktt/nks.toml`
1. Edit the values in the file as needed
1. Copy the `ktt-nks.service` file to `/etc/systemd/system/ktt-nks.service`
1. Start the service with `systemctl start ktt-nks`

### Deploying (bash commands)

```bash
adduser --no-create-home --disabled-login ktt-nks
cp keyless-tls-terminator /usr/local/bin/ktt
mkdir -p /etc/ktt
cp nks.toml.example /etc/ktt/nks.toml
chown -R ktt-nks:ktt-nks /etc/ktt
cp ktt-nks.service /etc/systemd/system/ktt-nks.service
systemctl start ktt-nks
```

## License

This repository and all code in it is licensed under the MIT license, available [here](https://gitlab.com/space55/keyless-tls-server-deployment/-/blob/main/LICENSE)
